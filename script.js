//Contar cuantas veces se repite un número
function arreglo() {
    var tamaño = document.getElementById('tamano').value;
    var arreglo = [];

    for (let i = 0; i < tamaño; i++) {
        arreglo.push(Math.floor(Math.random() * 10));
    }
    console.log(arreglo);
    var contador = arreglo.reduce((a, b) => {
        a[b] = (a[b] || 0) + 1;
        return a;
    }, {});
    console.log(Object.assign(contador));
}

function caritas() {
    //: ;
    var myArray = [':~(', ':->', ':~>', ';~D', ';~)', ':>', ';('];

    var caritas = myArray.filter(a => a.charAt(0) == (':', ';') || a.charAt(1) == ('-', '~') && a.charAt(2) == ('-', '~'));
    console.log(caritas.length + ' :(');

}
function botones() {
    var boton = prompt('Ingresa n de botones');

    for (var a = 0; a < boton; a++) {

        var button = document.createElement("button");
        button.textContent = a;
        var div = document.getElementById('div');
        div.appendChild(button);
    }
}

function holaMundo() {
    var myArray = ['Hola mundo'];

    for (let x = 0; x < myArray[0].length; x++) {
        console.log(myArray[0].charAt(x));
    }


}


function caritas2() {
    var myArray = [':~(', ':->', ':~>', ';~D', ';~)', ':>', ';('];
    // : ;
    // - ~
    // ) D
    //charAt obtiene caracter

    var guardar = myArray.filter((a) => a.includes("D") || a.includes(")"));
    console.log(guardar);

}

function ordenarMaMe() {
    var array2 = [1, 8, 2, 32, 9, 7, 4];
    array2.sort((a, b) => a - b);
    console.log(array2);
}


function crearBtn() {
    for (let x = 0; x < 10; x++) {
        var btn = document.createElement("button");
        btn.textContent = x;
        var div = document.getElementById("pintarBtn");
        div.appendChild(btn);
    }
}

function $(selector) {
    return document.getElementById("selector");
}

//ingresar "abcd" => "A-Bb-Ccc-Dddd"
function juntarletras(s) {
    var contador = "";
    contador += s.charAt(0).toUpperCase();
    for (let y = 1; y < 5; y++) {
        if (s.indexOf(s.charAt(y))) contador += s.charAt(y).toUpperCase() + s.charAt(y).toLowerCase().repeat(y);
    }
    console.log(contador);
}

//ingresar "abcd" => "A-Bb-Ccc-Dddd"
function accum(s) {
    return s.split('').map((c, i) => (c.toUpperCase() + c.toLowerCase().repeat(i))).join('-');
}

//0-1-1-2-3-5-8-13....
function fibonacci() {
    var oldNum = 0;
    var newNum = 1;
    var r = 1;
    var div = document.getElementById("pintarTabla");
    var tabla = document.createElement("table");

    for (let x = 0; x < 10; x++) {
        let tr = document.createElement("tr");
        let td = document.createElement("td");
        td.textContent = 0;
        tr.appendChild(td);
        td.textContent = r;
        tr.appendChild(td);
        tabla.appendChild(tr);

        r = oldNum + newNum;
        oldNum = newNum;
        newNum = r;
    }
    div.appendChild(tabla);
}

//      *
//     ***
//    *****
//   *******
//  *********
//     ***
//     ***
function flechaConsola() {
    var myFlecha = [" ", " ", " ", " ", " ", " ", " ", " ", " "];
    var baseFecha = [" ", " ", " ", "*", "*", "*"];
    var disminuir = 5;
    var aumentar = 5;
    for (let x = 0; x < 5; x++) {
        myFlecha[disminuir] = "*";
        myFlecha[aumentar] = "*";
        disminuir = disminuir - 1;
        aumentar = aumentar + 1
        console.log(myFlecha.join(''));
    }
    for (let x = 0; x < 2; x++) console.log(baseFecha.join(''));
}

// true si hay xxoo no importa orden
//si hay mas false
//si no aparece ninguna true
function xxoo(cadena) {
    var status = cadena.toLowerCase().split('').reduce((a, b) => {
        a[b] = (a[b] || 0) + 1;
        return a;
    }, {});
    if (((status.x == (undefined||2)) && (status.o == (undefined||2)))) return true;
    else return false;
}

//xoxo hecho por un chino
function XO(cadena) {
    return cadena.toLowerCase().split('x').length == cadena.toLowerCase().split('o').length;
}


